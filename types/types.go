package types

import (
	"encoding/json"
	"fmt"
	"github.com/aler9/goroslib"
	"github.com/aler9/goroslib/pkg/msg"
	"math/rand"
	"os"
	"time"
)

const (
	Topic   string = "pji_topic"
	Reset   string = "/reset"
	Rename  string = "/rename"
	TX      int    = 150
	TY      int    = 100
	Cmd_vel        = "/turtle1/cmd_vel"
)

type Linear struct {
	X float64
	Y float64
	Z float64
}

type Angular struct {
	X float64
	Y float64
	Z float64
}
type TwistData struct {
	Linear  []float64
	Angular []float64
}

func NewLinear(data []float64) *Linear {
	return &Linear{
		X: data[0],
		Y: data[1],
		Z: data[2],
	}
}
func NewAngular(data []float64) *Angular {
	return &Angular{
		X: data[0],
		Y: data[1],
		Z: data[2],
	}
}
func LoadCoord(jsonFile string) (tData TwistData) {
	data, err := os.Open(jsonFile)
	if err == nil {
		defer data.Close()
		decoder := json.NewDecoder(data)
		err = decoder.Decode(&tData)
	} else {
		fmt.Println(err.Error())
	}
	return
}

type Message struct {
	msg.Package `ros:"types"`
	Number      int32
}

// Definition of the services

type ResetServiceRequest struct {
	Coord
}
type RenameServiceRequest struct {
	Name string
}
type RenameServiceResponse struct {
	Car
}
type ResetServiceResponse struct {
	Car
}

// ResetService struct is enough to define a service no need to use .srv files
type ResetService struct {
	msg.Package `ros:"types"`
	ResetServiceRequest
	ResetServiceResponse
}

// RenameService struct is enough to define a service no need to use .srv files
type RenameService struct {
	msg.Package `ros:"types"`
	RenameServiceRequest
	RenameServiceResponse
}

// OnResetService this function is the callback function call when a resetService is executed
func OnResetService(req *ResetServiceRequest) (*ResetServiceResponse, bool) {
	CarOne.reset(req.Coord)
	return &ResetServiceResponse{*CarOne},
		true
}

// OnRenameService this function is the callback function call when a renameService is executed
func OnRenameService(req *RenameServiceRequest) (*RenameServiceResponse, bool) {
	CarOne.Name = req.Name
	return &RenameServiceResponse{
			*CarOne,
		},
		true
}

// The car's infos

type Car struct {
	Dir  Direction
	Name string
	Coord
	Dx float64
	Dy float64
}
type Direction struct {
	Dir string
}
type Coord struct {
	X float64
	Y float64
}
type Move struct {
	Dir Direction
	Coord
}

//CarOne the unique car at the start of server node
var CarOne *Car

// Definition of the move action

type ActionGoal struct {
	Move
}

type ActionResult struct {
	Car
}
type ActionFeedBack struct {
	Feedback string
}

type CarMoveAction struct {
	msg.Package `ros:"shared_actions"`
	ActionGoal
	ActionResult
	ActionFeedBack
}

func GoalExecute(as *goroslib.SimpleActionServer, goal *ActionGoal) {
	as.PublishFeedback(&ActionFeedBack{
		Feedback: "move success",
	})
	as.SetSucceeded(CarOne.moveProcess(goal.Coord))
}

func (car *Car) moveProcess(coord Coord) *ActionResult {
	//fmt.Println("old coord of the car ", car.Coord)
	car.Dx = coord.X
	car.X += car.Dx
	if car.X < 0 {
		car.X = 0
		car.Dx = float64(rand.Intn(TX))
	}
	if car.X >= float64(TX) {
		car.X = float64(TX) - 1
		car.Dx = float64(rand.Intn(TX))
	}
	car.Dy += coord.Y
	car.Y += car.Dy
	if car.Y < 0 {
		car.Y = 0
		car.Dy = float64(rand.Intn(TY))
	}
	if car.Y >= float64(TY) {
		car.Y = float64(TY) - 1
		car.Dy = float64(rand.Intn(TY))
	}
	return &ActionResult{
		*car,
	}
}
func (car *Car) reset(coord Coord) {
	car.X = coord.X
	car.Y = coord.Y
	car.Dir = Direction{"NORTH"}
	car.Name = "my_car"
}

var Directions = [4]Direction{{"EAST"}, {"WEST"}, {"NORTH"}, {"SOUTH"}}

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
	CarOne = &Car{Directions[rand.Intn(4)],
		"car1",
		Coord{
			X: float64(rand.Intn(TX)),
			Y: float64(rand.Intn(TY)),
		},
		float64(rand.Intn(TX)),
		float64(rand.Intn(TY)),
	}
}
