package main

import (
	"fmt"
	"my_apps/types"

	"github.com/aler9/goroslib"
)

func main() {
	n, err := goroslib.NewNode(goroslib.NodeConf{
		Name:          "reset_service",
		MasterAddress: "127.0.0.1:11311",
	})
	if err != nil {
		panic(err)
	}
	defer n.Close()

	sc, err := goroslib.NewServiceClient(goroslib.ServiceClientConf{
		Node: n,
		Name: types.Reset,
		Srv:  &types.ResetService{},
	})
	if err != nil {
		panic(err)
	}
	defer sc.Close()

	req := types.ResetServiceRequest{}
	res := types.ResetServiceResponse{}
	err = sc.Call(&req, &res)
	if err != nil {
		panic(err)
	}

	fmt.Printf("response: Name: %v Direction: %v x= %v y= %v\n", res.Name, res.Dir.Dir, res.X, res.Y)
}
