package main

import (
	"fmt"
	"my_apps/types"
	"os"

	"github.com/aler9/goroslib"
)

func main() {

	n, err := goroslib.NewNode(goroslib.NodeConf{
		Name:          "rename_service",
		MasterAddress: "127.0.0.1:11311",
	})
	if err != nil {
		panic(err)
	}
	defer n.Close()

	sc, err := goroslib.NewServiceClient(goroslib.ServiceClientConf{
		Node: n,
		Name: types.Rename,
		Srv:  &types.RenameService{},
	})
	if err != nil {
		panic(err)
	}
	defer sc.Close()

	newName := os.Args[1]

	req := types.RenameServiceRequest{
		Name: newName,
	}
	res := types.RenameServiceResponse{}
	err = sc.Call(&req, &res)
	if err != nil {
		panic(err)
	}

	fmt.Printf("response: Name: %v Direction: %v x= %v y= %v\n", res.Name, res.Dir.Dir, res.X, res.Y)

}
