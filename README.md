### ROS1 in Golang using goroslib

### Goal of the pji: 

The goal of the project was to developpe an application in ROS2 using Go language.

Without finding a go library for ROS2 that support at least the basic features of ros2 like: publishing, subscribing, services, actions; we decide 
to move in ROS1, which one I found a library that provide a set of ros1 features.


Here is the documentation of the library : github.com/aler9/goroslib or https://pkg.go.dev/github.com/aler9/goroslib#section-readme

### INSTALL:
1. Install Go language: https://go.dev/doc/install
2. Install ROS1 noetic : http://wiki.ros.org/noetic/Installation
3. Clone the repos from gitlab: https://gitlab.univ-lille.fr/elhadjabdoulaye.diallo.etu/pji_ros1_go

### RUN:
To test one of the features, here are the steps: 

1. Open a new terminal then run the command **roscore** to launch the ros1's master.
2. Open another terminal move to pji_ros1_go repository and then run the command: **go run .** to run the server node.
3. To test one of the ros1's features like service, action or publisher, move to the specific repos and then by this 
command **go run <file_name.go>**

### EXAMPLES:

1. ~$ roscore
You will see something like this :

![roscore!](roscore.png "roscore")

2. Launch the server node: 
![server_node!](server.png )
3. To test a service feature, I defined a car that can move in a specific area, thus the car has a coordinate x,y, a direction and a name.
So it's possible to rename the Car by using the renameService in the package serviceClient
 ![rename!](renameS.png)
4. The reset service updated the Car's coordinates to zero.
 ![reset!](reset.png)
5. To test action feature, I define a function that move the Car in random location in side the area.
![action!](move_ac.png)
6. Now another real example with publisher between the go code and the turtlesim.
   1. Launch turtlesim:
      ![turtlesim!](turtlesim.png)
      You will see then
      ![Turtlesim!](Turtle.png)
   2. Now by using the Go code, I will make the move by publishing somme data on the topic /turtle1/cmd_vel. 
   In a new terminal move to the actionclient package and then run the commad: **go run turtlePublisher.go data.json**
   
   And then can see the turtle moving a round, you can find a video in the repos.

This last example shows that, we can easily send write an efficient Go code for ros1.

### Next steps: 
As it says in the beginning, the purpose of the project is to experiment the Go language in ROS2, that I started to do before experimenting the goroslib.

Despite the existence of some rclgo but none of them help me to experiment at one example in ROS2.

Here is the list:

# rclgo by juaruipav

After Forking, clone the repos by running the command: git clone https://github.com/juaruipav/rclgo.git

The requirement for this repos is that the ros2 distro should be bouncy,

I already tried some changes on the repos but, still have an errors related to some header's c file.

move in the publisher directory then run this command:

go test

## rclgo by tiiuae

To use this rcl, the galactic ros2 distro is required.

After Forking, clone the repos by running the command: git clone https://github.com/tiiuae/rclgo.git

move in the custom_message_package directory and then run these command:

colcon build

. install/local_setup.sh

move in the greeter directory then run these commands :

go generate

go build to compile and then ./greeter

After installing the galactic distro, this rclgo work fine.


### Comments

I made the choice for this project for 2 reasons: 

1. I wanted to have a look (even not so deep) in robotic field, to know the basics features
2. I wanted to do something new for me and learn a new programming language, that's why I bought a book in Go language titled **Pro Go** written by **Adam Freeman**
it's a huge book with 38 chapters but very good.

Go language is an efficient a reliable programming language, it's become more than more popular by its simplicity.

Go can be used for just about any programming task but is best suited to server development or system development.
The extensive stantard library includes support for most common server-side tasks, such as handling HTTP requests, accessing SQL databases, and rendering HTML templates. it has excellent threading support, and a comprehensive reflection system
makes it possible to write flexible APIs for platforms and frameworks.

Go is cross-platform, we can write on Windows, for example and deploy on Linux servers.



    




