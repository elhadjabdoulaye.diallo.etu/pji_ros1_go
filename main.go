package main

import (
	"github.com/aler9/goroslib"
	"my_apps/types"
	"os"
	"os/signal"
)

func main() {
	node, err := goroslib.NewNode(goroslib.NodeConf{
		Name:          "server",
		MasterAddress: "127.0.0.1:11311",
	})
	if err != nil {
		panic(err)
	}
	defer node.Close()

	// create a reset service provider
	sp, err := goroslib.NewServiceProvider(goroslib.ServiceProviderConf{
		Node:     node,
		Name:     types.Reset,
		Srv:      &types.ResetService{},
		Callback: types.OnResetService,
	})
	if err != nil {
		panic(err)
	}
	defer sp.Close()

	// create a reset service provider
	rp, err := goroslib.NewServiceProvider(goroslib.ServiceProviderConf{
		Node:     node,
		Name:     types.Rename,
		Srv:      &types.RenameService{},
		Callback: types.OnRenameService,
	})
	if err != nil {
		panic(err)
	}
	defer rp.Close()

	as, err := goroslib.NewSimpleActionServer(goroslib.SimpleActionServerConf{
		Node:      node,
		Name:      "/move",
		Action:    &types.CarMoveAction{},
		OnExecute: types.GoalExecute,
	})
	if err != nil {
		panic(err)
	}
	defer as.Close()
	// wait for CTRL-C
	channel := make(chan os.Signal, 1)
	signal.Notify(channel, os.Interrupt)
	<-channel
}
