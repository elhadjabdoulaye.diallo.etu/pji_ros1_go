package main

import (
	"fmt"
	"github.com/aler9/goroslib"
	"my_apps/types"
	"os"
	"os/signal"
)

func onMessage(msg *types.Message) {
	fmt.Println("message received: ", msg.Number)
}
func main() {
	sNode, err := goroslib.NewNode(goroslib.NodeConf{
		Name:          "subscriber",
		MasterAddress: "127.0.0.1:11311",
	})

	if err != nil {
		panic(err)
	}
	defer sNode.Close()

	sub, err := goroslib.NewSubscriber(goroslib.SubscriberConf{
		Node:     sNode,
		Topic:    types.Topic,
		Callback: onMessage,
	})
	if err != nil {
		panic(err)
	}

	defer sub.Close()

	// wait for CTRL-C
	channel := make(chan os.Signal, 1)
	signal.Notify(channel, os.Interrupt)
	<-channel
}
