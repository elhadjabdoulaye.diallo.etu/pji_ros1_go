package main

import (
	"fmt"
	"github.com/aler9/goroslib"
	"my_apps/types"
	"os"
	"os/signal"
	"strconv"
)

func main() {
	node, err := goroslib.NewNode(goroslib.NodeConf{
		Name:          "move_action",
		MasterAddress: "127.0.0.1:11311",
	})

	if err != nil {
		panic(err)
	}
	defer node.Close()
	ac, err := goroslib.NewSimpleActionClient(goroslib.SimpleActionClientConf{
		Node:   node,
		Name:   "/move",
		Action: &types.CarMoveAction{},
	})
	if err != nil {
		panic(err)
	}
	defer ac.Close()

	ac.WaitForServer()

	done := make(chan struct{})
	dir := types.Direction{Dir: os.Args[1]}
	x, _ := strconv.ParseFloat(os.Args[2], 64)
	y, _ := strconv.ParseFloat(os.Args[3], 64)
	newMove := types.Move{
		Dir: dir,
		Coord: types.Coord{
			X: x,
			Y: y,
		},
	}
	//sending goal
	err = ac.SendGoal(goroslib.SimpleActionClientGoalConf{
		Goal: &types.ActionGoal{
			Move: newMove,
		},
		OnDone: func(state goroslib.SimpleActionClientGoalState, res *types.ActionResult) {
			fmt.Printf("response: Name: %v Direction: %v x= %.2f y= %.2f\n", res.Name, res.Dir.Dir, res.X, res.Y)
			close(done)
		},
		OnFeedback: func(fb *types.ActionFeedBack) {
			fmt.Println("feedback", fb.Feedback)
		},
	})
	if err != nil {
		panic(err)
	}
	channel := make(chan os.Signal, 1)
	signal.Notify(channel, os.Interrupt)

	select {
	case <-done:
	//
	case <-channel:
	}
}
