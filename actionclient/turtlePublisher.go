package main

import (
	"fmt"
	"github.com/aler9/goroslib"
	"github.com/aler9/goroslib/pkg/msgs/geometry_msgs"
	"my_apps/types"
	"os"
	"time"
)

func publishMsg(numberChan chan<- int32) {
	tickChan := time.Tick(time.Second)
	var index int32 = 0

	for {
		<-tickChan
		numberChan <- index
		index++
	}
}

func main() {
	node, err := goroslib.NewNode(goroslib.NodeConf{
		Name:          "move_action",
		MasterAddress: "127.0.0.1:11311",
	})

	if err != nil {
		panic(err)
	}
	defer node.Close()

	pub, err := goroslib.NewPublisher(goroslib.PublisherConf{
		Node:  node,
		Topic: types.Cmd_vel,
		Msg:   &geometry_msgs.Twist{},
	})
	if err != nil {
		panic(err)
	}
	defer pub.Close()
	numChan := make(chan int32)
	data := types.LoadCoord(os.Args[1])
	linear := types.NewLinear(data.Linear)
	angular := types.NewLinear(data.Angular)
	go publishMsg(numChan)
	for msg := range numChan {
		fmt.Println("message published : ", msg)
		msg := &geometry_msgs.Twist{
			Linear: geometry_msgs.Vector3{X: linear.X, Y: linear.Y, Z: linear.Z},
			Angular: geometry_msgs.Vector3{
				X: angular.X,
				Y: angular.Y,
				Z: angular.Z,
			},
		}
		fmt.Println("msg published: ", msg.Linear, msg.Angular)
		pub.Write(msg)
	}
}
