module my_apps

go 1.18

require github.com/aler9/goroslib v0.0.0-20220601104512-f6c16e5b54d3

require (
	github.com/gookit/color v1.5.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/sys v0.0.0-20210502180810-71e4cd670f79 // indirect
)
